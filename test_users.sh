#!/bin/sh
if [ "$#" -ne 1 ]
then
    echo "usage:\n\t'sh test_user.sh projnumber\n\tBuilds a report.txt file of user passings/failings in this directory. Arguments:\n\t\tprojnumber: The nand2tetris project to test against, e.g use the argument 01 to test against the first nand2tetris project. Requires the moodle download zip file to be paced into the submissions folder."
    exit
fi
# make a tmp directory to dump working files
mkdir tmp
# unzip submissions file
unzip -q submissions/*.zip -d tmp/submissions

ls tmp/submissions > tmp/submissionfolders.txt

# clear results file
echo "" > results.txt
# for each user who submitted (from submissionfolders.txt)
while read userfolder
do
    IFS='_' read -ra username <<< "$userfolder" #extract username from submission for easier reading
    echo $username >> results.txt
    echo "processing $username"

    unzip -q tmp/submissions/"$userfolder"/*.zip -d tmp/submissions/"$userfolder" 2>> results.txt

    # copy only the user hdl files into temporary userfiles folder for testing
    mkdir tmp/userfiles
    # cp tmp/submissions/"$userfolder"/**/*.hdl tmp/userfiles # only works with zsh
    find tmp/submissions/"$userfolder" -name \*.hdl -exec cp {} tmp/userfiles \;

    # copy nand2tetris source files for comparison
    find nand2tetris_src/projects/$1 -name \*.tst -exec cp {} tmp/userfiles \;
    find nand2tetris_src/projects/$1 -name \*.cmp -exec cp {} tmp/userfiles \;

    # cp nand2tetris_src/projects/$1/*.tst tmp/userfiles
    # cp nand2tetris_src/projects/$1/*.cmp tmp/userfiles
    # test against all nand2tetris .tst files from source folder
    # ls tmp/userfiles

    for test in `find tmp/userfiles -name "*.tst"`
    do
        chipname=$(echo $test | cut -c '15-' | rev | cut -c '5-' | rev) #remove the first 14 characters and the last 4 to extract chip name
        printf "\t$chipname: " >> results.txt
        if [ ! -f tmp/userfiles/$chipname.hdl ]
        then
            echo "file not submitted" >> results.txt
        else
            sh nand2tetris_src/hardwareSimulator/HardwareSimulator.sh $test >> results.txt 2>> results.txt #both stdout and stderr go into results
        fi
    done
    # remove userfiles
    rm -rf tmp/userfiles
done<tmp/submissionfolders.txt

# remove tmp directory
rm -rf tmp
